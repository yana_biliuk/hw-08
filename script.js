// task 1

const arr = ["travel", "hello", "eat", "ski", "lift" ]

let value = 0;

for (let i = 0; i < arr.length; i++) {
    if (arr[i].length > 3) {
        value++;
    }
}

console.log(value);

// task 2

const people = [
  { name: "Іван", age: 25, sex: "чоловіча" },
  { name: "Анна", age: 25, sex: "жіноча" },
  { name: "Петро", age: 40, sex: "чоловіча" },
  { name: "Олена", age: 35, sex: "жіноча" },
];

const manPeople = people.filter((value) => value.sex === "чоловіча");
console.log(manPeople);
